import os
from setuptools import setup

README = open(os.path.join(os.path.dirname(__file__), 'README.rst')).read()

setup(
    name = 'django-localflavor-kz',
    version = '0.1',
    description = 'Country-specific Django helpers for Republic of Kazakhstan',
    long_description = README,
    author = 'Dauren Tatubaev',
    author_email = 'dauren@aworks.kz',
    license='BSD',
    url = 'bitbucket.org/aworks/django-localflavor-kz',
    packages = ['django_localflavor_kz'],
    include_package_data = True,
    classifiers = [
        'Development Status :: 3 - Alpha',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
    ],
    install_requires=[
        'Django>=1.5',
    ]
)
