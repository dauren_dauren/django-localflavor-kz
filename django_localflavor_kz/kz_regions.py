# -*- encoding: utf-8 -*-
"""
Vocabulary:
    Region - Oblast' or a Republican city (ie Astana and Almaty)
    District - Raion

Sources:
    http://en.wikipedia.org/wiki/Provinces_of_Kazakhstan
    http://en.wikipedia.org/wiki/Districts_of_Kazakhstan
    http://www.geonames.de/coukz-sub.html
    http://www.worldstatesmen.org/Kazakh_admin.html
"""
from django.utils.translation import ugettext_lazy as _

# Special regions, not part of Oblast. Similar to D.C. in USA or D.F. in Mexico
KZ_SPECIAL_REGION_CHOICES = (
    (0, _('Astana City')),
    (1, _('Almaty City')),
)

# Oblasts (one level down from Republic of Kazakhstan. oblasts comprise a republic)
KZ_OBLAST_CHOICES = (
    # starting from 2 since first two are republican cities
    (2, _('Akmola')),
    (3, _('Almaty')),
)

KZ_REGION_CHOICES = KZ_OBLAST_CHOICES + KZ_SPECIAL_REGION_CHOICES

# Raion (one level down from Oblast. Raions comprise an oblast)
KZ_DISTRICT_CHOICES = (
)

KZ_CITY_CHOICES = (
)

KZ_REGION_CHOICES_NORMALIZED = {
    'ak': 'AK',
    'al': 'AL',
    'ala': 'AL',
    'alabama': 'AL',
    'alaska': 'AK',
    'american samao': 'AS',
    'american samoa': 'AS',
}

KZ_DISTRICT_CHOICES_NORMALIZED = {

}


KZ_LICENSE_PLATE_REGIONS = (
    (1, _('Astana')),
    (2, _('Almaty')),
    (3, _('Aktobe')),

    '''
    1 - Астана
    2 - Алматы
    3 - Акмолинская область
    4 - Актюбинская область
    5 - Алматинская область
    6 - Атырауская область
    7 - Восточно-Казахстанская область
    8 - Жамбылская область
    9 - Западно-Казахстанская область
    10 - Карагандинская область
    11 - Костанайская область
    12 - Кызылординская область
    13 - Мангыстауская область
    14 - Павлодарская область
    15 - Северо-Казахстанская область
    16 - Южно-Казахстанская область
    '''
)


KZ_ABBREVIATIONS = {
    '''
    ИИК - ЖСК (жеке сјйкестендіру коды)
    Кбе - Кбе (бенефициар коды)
    БИК - БСК (бизнес сјйкестендіру коды)
    БИН - БСН (бизнес сјйкестендіру нґмірі)
    ИИН - ЖСН (жеке сјйкестендіру нґмірі)
    '''
}
