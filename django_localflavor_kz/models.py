from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.db.models.fields import CharField
from django_localflavor_kz.kz_regions import KZ_REGION_CHOICES, KZ_DISTRICT_CHOICES


'''
'''
class KZRegionField(CharField):

    description = _("Region of Kazakhstan")

    def __init__(self, *args, **kwargs):
        kwargs['choices'] = KZ_REGION_CHOICES
        #kwargs['max_length'] = 2
        super(KZRegionField, self).__init__(*args, **kwargs)

'''
'''
class KZDistrictField(CharField):

    description = _("District of Kazakhstan")

    def __init__(self, *args, **kwargs):
        kwargs['choices'] = KZ_DISTRICT_CHOICES
        #kwargs['max_length'] = 2
        super(KZDistrictField, self).__init__(*args, **kwargs)


'''
'''
class KZPhoneNumberField(CharField):

    description = _("Phone number")

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 20
        super(KZPhoneNumberField, self).__init__(*args, **kwargs)

    def formfield(self, **kwargs):
        from django_localflavor_kz.forms import KZPhoneNumberField as kz_phone_field
        defaults = {'form_class': kz_phone_field}
        defaults.update(kwargs)
        return super(KZPhoneNumberField, self).formfield(**defaults)
