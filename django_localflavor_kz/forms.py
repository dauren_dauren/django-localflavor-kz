"""
Kazakhstan-specific forms helpers
"""
from __future__ import absolute_import, unicode_literals

import re

from django_localflavor_kz.kz_regions import KZ_REGION_CHOICES, KZ_DISTRICT_CHOICES
from django.forms.fields import RegexField, Select, CharField
from django.utils.translation import ugettext_lazy as _
from django.core.validators import EMPTY_VALUES
from django.utils.encoding import smart_text
from django.forms import ValidationError

phone_digits_re = re.compile(r'^(?:[78]-?)?(\d{3})[-\.]?(\d{3})[-\.]?(\d{4})$')

'''
'''
class KZRegionSelect(Select):
    """
    A Select widget that uses a list of Kazakh Regions (Oblasts) as its choices
    """
    def __init__(self, attrs=None):
        super(KZRegionSelect, self).__init__(attrs, choices=KZ_REGION_CHOICES)

'''
'''
class KZDistrictSelect(Select):
    """
    A Select widget that uses a list of Kazakh Districts (Raions) as its choices.
    """
    def __init__(self, attrs=None):
        super(KZDistrictSelect, self).__init__(attrs, choices=KZ_DISTRICT_CHOICES)


'''
'''
class KZPostalCodeField(RegexField):
    """
    Kazakh Postal code field.
    Format: XXXXXX, where X is any digit, and first digit is not zero.
    """
    default_error_messages = {
        'invalid': _('Enter a postal code in the format XXXXXX.'),
    }

    def __init__(self, max_length=None, min_length=None, *args, **kwargs):
        super(KZPostalCodeField, self).__init__(r'^\d{6}$', max_length, min_length, *args, **kwargs)

'''
'''
class KZPhoneNumberField(CharField):
    default_error_messages = {
        'invalid': _('Phone numbers must be in XXX-XXX-XXXX format.'),
    }

    def clean(self, value):
        super(KZPhoneNumberField, self).clean(value)
        if value in EMPTY_VALUES:
            return ''
        value = re.sub('(\(|\)|\s+)', '', smart_text(value))
        m = phone_digits_re.search(value)
        if m:
            return '%s-%s-%s' % (m.group(1), m.group(2), m.group(3))
        raise ValidationError(self.error_messages['invalid'])

'''
'''
class KZPassportNumberField(RegexField):
    """
    Kazakhstan internal passport number format:
    XXXX XXXXXX where X - any digit.
    """
    default_error_messages = {
        'invalid': _('Enter a passport number in the format XXXX XXXXXX.'),
    }
    def __init__(self, max_length=None, min_length=None, *args, **kwargs):
        super(KZPassportNumberField, self).__init__(r'^\d{4} \d{6}$', max_length, min_length, *args, **kwargs)


'''
'''
class KZAlienPassportNumberField(RegexField):
    """
    Kazakhstan alien's passport (Vid na zhitelstvo) number format:
    XX XXXXXXX where X - any digit.
    """
    default_error_messages = {
        'invalid': _('Enter a passport number in the format XX XXXXXXX.'),
    }
    def __init__(self, max_length=None, min_length=None, *args, **kwargs):
        super(KZAlienPassportNumberField, self).__init__(r'^\d{2} \d{7}$', max_length, min_length, *args, **kwargs)


'''
'''
class KZNNNField(RegexField):
    """
    A field format for:
        IIN (Individual Identification Number)
        BIN (Business Identification Number)

    Its typically a 12 digit number in format XXXXXXXXXXXX where (for IIN):

    a) first six digits are date of birth in format YYMMDD
    b) 7th digit is century born + sex field (odd - female, even - male, 1/2 - 19th century, 3/4 - 20th, 5/6 - 21st)
    c) 8-11th - department of justice serial number
    d) 12th - control number
    """
    default_error_messages = {
        'invalid': _('Enter a Identification Number number in the format XXXXXXXXXXXX.'),
    }
    def __init__(self, max_length=None, min_length=None, *args, **kwargs):
        super(KZNNNField, self).__init__(r'^\d{12}}$', max_length, min_length, *args, **kwargs)